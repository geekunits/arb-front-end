$(function() {

	// Custom JS
	$window = $(window);
	$document = $(document);
	$body = $('body');

	function formatCurrency(currency) {
		var $option = $(
			'<span><span class="currency-select-dropdown-img"><img src="img/currencies/' + currency.id + '.png" /></span><span class="currency-select-dropdown-text">' + currency.text + '</span></span>'
			);
		return $option;
	};

	$('.js-apply-select2').select2({
		minimumResultsForSearch: -1,
		templateResult: formatCurrency,
		templateSelection: formatCurrency,
		formatSelection: formatCurrency,
		formatResult: formatCurrency,
		dropdownCssClass: 'topup__currency-select-dropdown',
		containerCssClass: 'topup__currency-select'
	});

	transactionHistoryTableScrollbar = {
		state: false,
		$element: $('.trans-history__table-scroll-container'),
		init: function() {
			this.$element.mCustomScrollbar({axis: 'x'});
			this.state = true;
		},
		destroy: function() {
			this.$element.mCustomScrollbar("destroy");
			this.state = false;
		}
	};

	if ($window.width() <= 991) {
		transactionHistoryTableScrollbar.init();
	}

	$window.on('resize', function() {
		if (!transactionHistoryTableScrollbar.state && $window.width() <= 991) {
			transactionHistoryTableScrollbar.init();
		} else if (transactionHistoryTableScrollbar.state && $window.width() > 991) {
			transactionHistoryTableScrollbar.destroy();
		}

		if ( 
			(mobileNavigation.$element.attr('style') || mobileUserNavigation.$element.attr('style'))
			&& $window.width() > 768
			) {
			mobileNavigation.hide();
		mobileUserNavigation.hide();
		setTimeout(function() {
			mobileNavigation.$element.removeAttr('style')
			mobileUserNavigation.$element.removeAttr('style');
		}, 150);
	}
});

	var mobileNavigation = {
		state: false,
		$element: $('.header__panel'),
		show: function() {
			mobileUserNavigation.hide();
			this.state = true;
			$body.addClass('mobile-nav-active');
			this.$element.fadeIn(100);
		},
		hide: function() {
			this.state = false;
			$body.removeClass('mobile-nav-active');
			this.$element.fadeOut(100);
		},
	};

	$('a.header__mobile-burger').on('click', function() {
		if (mobileNavigation.state) {
			mobileNavigation.hide();
			return;
		}
		mobileNavigation.show();
	});

	var mobileUserNavigation = {
		state: false,
		$element: $('.user-aside-container'),
		show: function() {
			mobileNavigation.hide();
			this.state = true;
			this.$element.fadeIn(100);
			$body.addClass('mobile-user-nav-active');
		},
		hide: function() {
			this.state = false,
			this.$element.fadeOut(100);
			$body.removeClass('mobile-user-nav-active');
		}
	};

	$('.js-toggle-mobile-user-nav').on('click', function() {
		mobileUserNavigation.show();
	});

	$('.js-close-user-nav').on('click', function() {
		mobileUserNavigation.hide();
	})

	// particles
	if ( $('#particlesCanvas').length ){
		function initParticles() {
			var canvas = document.getElementById('particlesCanvas');
			var ctx = canvas.getContext('2d');
			
			function getCoords(xPercentage, yPercentage) {
				var canvasWidth = canvas.getBoundingClientRect().width;
				var canvasHeight = canvas.getBoundingClientRect().height;

				var newX = canvasWidth/100 * xPercentage;
				var newY = canvasHeight/100 * yPercentage;

				return [newX, newY];
			}

			var distance = .2;
			
			var circles = [
			{
				x: getCoords(1.5, 5)[0],
				y: getCoords(1.5, 5)[1],
				radius: 3.6,
				startColor: '#59D158',
				endColor: '#EBEBEB',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			{
				x: getCoords(10, 95)[0],
				y: getCoords(10, 95)[1],
				radius: 3.3,
				startColor: '#EBEBEB',
				endColor: '#EBEBEB',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			{
				x: getCoords(26, 7)[0],
				y: getCoords(26, 7)[1],
				radius: 2.9,
				startColor: '#EBEBEB',
				endColor: '#EBEBEB',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			{
				x: getCoords(32, 66)[0],
				y: getCoords(32, 66)[1],
				radius: 2.5,
				startColor: '#90B0FB',
				endColor: '#B99DFE',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			{
				x: getCoords(50, 12)[0],
				y: getCoords(50, 12)[1],
				radius: 2.5,
				startColor: '#59D158',
				endColor: '#EBEBEB',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			{
				x: getCoords(55, 86)[0],
				y: getCoords(55, 86)[1],
				radius: 2.5,
				startColor: '#90B0FB',
				endColor: '#B99DFE',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			{
				x: getCoords(72, 9)[0],
				y: getCoords(72, 9)[1],
				radius: 3.6,
				startColor: '#F990AF',
				endColor: '#E86294',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			{
				x: getCoords(87, 89)[0],
				y: getCoords(87, 89)[1],
				radius: 3.6,
				startColor: '#F990AF',
				endColor: '#E86294',
				xVelocity: getRandomArbitrary() > 0 ? distance : -distance,
				yVelocity: getRandomArbitrary() > 0 ? distance : -distance
			},
			];

			function draw() {
				ctx.clearRect(0,0, 500, 200);

				circles.forEach(function(circle) {
					var xdiff, ydiff;
					var newX, newY;

					xdiff = getRandomArbitrary() > 0 ? distance : -distance;
					ydiff = getRandomArbitrary() > 0 ? distance : -distance;

					if (circle.x - circle.radius <= 0 || circle.x + circle.radius >= canvas.width) {
						circle.xVelocity = -circle.xVelocity;
					}
					if (circle.y - circle.radius <= 0 || circle.y + circle.radius >= canvas.height) {
						circle.yVelocity = -circle.yVelocity;
					}

					circle.x = circle.x + circle.xVelocity;
					circle.y = circle.y + circle.yVelocity;

					ctx.beginPath();
					ctx.arc(
						circle.x, 
						circle.y, 
						circle.radius,
						0,
						2 * Math.PI, 
						false
						);

					var gradient = ctx.createLinearGradient(
						circle.x - circle.radius,
						circle.y - circle.radius,
						circle.x + circle.radius,
						circle.y - circle.radius
						);

					gradient.addColorStop(0, circle.startColor);
					gradient.addColorStop(1, circle.endColor);
					ctx.fillStyle = gradient;
					ctx.fill();
				})


				window.requestAnimationFrame(draw);
			}

			function getRandomArbitrary() {
				return Math.random() * (1 + 1) - 1;
			}
			
			draw();
		}

		initParticles();
	};
	
	// chart.js init
	if ( $('#chartjsCanvas').length ){
		var chartCanvas = document.getElementById('chartjsCanvas');
		chartCanvas.height = 89;
		var chartCtx = chartCanvas.getContext('2d');
		
		var chart = new Chart(chartCtx, {
				// The type of chart we want to create
				type: 'line',

				// The data for our dataset
				data: {
					labels: ["18.08", "19.08", "20.08", "21.08", "22.08"],
					datasets: [{
								// label: "My First dataset",
								pointBackgroundColor: 'rgb(255, 99, 132)',
								backgroundColor: 'rgb(255, 99, 132, 0)',
								borderColor: 'rgb(255, 99, 132)',
								data: [0, 22, 42, 64, 140],
							}]
						},

				// Configuration options go here
				options: {
					tooltips: {
						custom: function(tooltip) {
							if (!tooltip) return;
							// disable displaying the color box;
							tooltip.displayColors = false;
						},
						callbacks: {
							// use label callback to return the desired label
							label: function(tooltipItem, data) {
								return tooltipItem.xLabel + " - " + tooltipItem.yLabel + ' $';
							},
							// remove title
							title: function(tooltipItem, data) {
								return;
							}
						}
					},
					legend: {
						display: false,
					},
					scales: {
						yAxes: [{
							ticks: {
								stepSize: 80,
								suggestedMax: 240,
								fontSize: 8,
								fontFamily: "'Open Sans', sans-serif",
								fontColor: '#C1BDCA',
							},
							gridLines: {
								drawBorder: false,
							}
						}],
						xAxes: [{
							ticks: {
								fontSize: 8,
								fontFamily: "'Open Sans', sans-serif",
								fontColor: '#C1BDCA',
							},
							gridLines: {
								display: false,
							},
						}]
					}
				}
			});

		function padGraph() {
			$padEl = $('.dashboard__row--topup');
			$chart = $('#chartjsCanvas');
			$chartContainer = $('.balance__graph');
			topupHeight = $padEl.height()
			chartHeight = $chart.height();

			$chartContainer.css('bottom', (-1 * (topupHeight + chartHeight + 60)) + 'px');
			$padEl.css('margin-bottom', (chartHeight +  60) + 'px');
			console.log('pepes');
		}
		
		if ($window.width() <= 640) {
			padGraph();
		}

		$window.on('resize', function() {
			if ($window.width() <= 640) {
				padGraph();
			} else {
				$('.balance__graph, .dashboard__row--topup').removeAttr('style');
			}
		})
	};

	//
	// home-roadmap 
	//

	// $('.home-roadmap-owl').owlCarousel({
	// 	nav:false,         
	// 	items: 1,        
	// 	margin: 0,       
	// 	dots: false,       
	// 	loop: false
	// });

	//
	// home-roadmap ...end;
	//

	//
	// home-table-tabs 
	//

	var $categoriesNavBtns = $('.home-orders__table-nav-link');
	var $categoriesTableTabs = $('.home-orders__table-tab');

	$categoriesNavBtns.on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		var href = $this.attr('href');

		changeTabs($categoriesNavBtns, $this, href, $categoriesTableTabs);
	});


	var $tableBotNavBtn = $('.home-orders__table-bot-nav-link'); 

	$tableBotNavBtn.on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		var href = $this.attr('href');
		var $currentCategTab = $('.home-orders__table-tab.active');
		var $tableTabs = $currentCategTab.find('.home-orders__table-t');
		var $botNavBtns = $currentCategTab.find('.home-orders__table-bot-nav-link');

		changeTabs($botNavBtns, $this, href, $tableTabs);
	});

	function changeTabs($allBtns, $initiator, targetID, $allTabs){
		$allBtns.parent().removeClass('active');
		$allTabs.removeClass('active');

		$initiator.parent().addClass('active');
		$(targetID).addClass('active');
	};

	//
	// home--table-tabs ...end;
	//

	//
	// animation top bar
	//

	if ( $('.home-header__top-l-round-bar').length ) {

		var animateBars = function() {
			$('.home-header__top-l-round-bar').each(function(item) {
				if ($(this).hasClass('is-animated'))
					return;

				if (isElementInViewport($(this)[0])) {
					$(this).addClass('is-animated');
					var _ = $(this);
					setTimeout(function() {
						_.css('max-width', '100%');
					}, 150);
				}
			});
		}
		animateBars();

		$window.on('scroll', function() {
			animateBars();
		});
	}

	function isElementInViewport (el) {

		var rect = el.getBoundingClientRect();

		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && 
			rect.right <= (window.innerWidth || document.documentElement.clientWidth)
			);
	}

	//
	// animation top bar ... end;
	//

	//
	// home countdown
	//

	var $timer   = $('#home-header__top-m-timer');
	var $days    = $('#home-header__top-m-timer-days');
	var $hours   = $('#home-header__top-m-timer-hours');
	var $mins    = $('#home-header__top-m-timer-minutes');
	var $seconds = $('#home-header__top-m-timer-seconds');

	$timer.countdown('2018/10/09', function(event) {
		$days.html(event.strftime('%D'));
		$hours.html(event.strftime('%H'));
		$mins.html(event.strftime('%M'));
		$seconds.html(event.strftime('%S'));
	});

	//
	// home countdown ... end;
	//

	//
	// home roadmap;
	//

	var $owlRoadmap = $('.home-roadmap__scene-in');
	var owlRoadVals = {
		currItem : 0,
		items : 0
	};
	var roadTimer;

	$owlRoadmap.owlCarousel({
		nav:false,         
		items: 1,        
		margin: 430,       
		dots: true,       
		loop: false,
		autoWidth: true,
		smartSpeed: 1000,

		onInitialized: function(e){
			owlRoadVals.items = e.item.count;
			roadTimer = new RecurringTimer(changeRoadSlide, 5000);
		},

		onTranslated: function(e){
			owlRoadVals.currItem = e.item.index+1;
		}
	});

	$owlRoadmap.hover(function() {
		roadTimer.pause();
	}, function() {
		roadTimer.resume();
	});

	function RecurringTimer(callback, delay) {
		var timerId, start, remaining = delay;

		this.pause = function() {
			window.clearTimeout(timerId);
			remaining -= new Date() - start;
		};

		var resume = function() {
			start = new Date();
			timerId = window.setTimeout(function() {
				remaining = delay;
				resume();
				callback();
			}, remaining);
		};

		this.reset = function() {
			remaining = delay;
		};

		this.resume = resume;

		this.resume();
	};

	function changeRoadSlide(){
		if ( owlRoadVals.items == owlRoadVals.currItem ) 
			$owlRoadmap.trigger('to.owl.carousel', [0]);
		else 
			$owlRoadmap.trigger('next.owl.carousel');
	};

	//
	// home roadmap ... end;
	//

	//
	// fixed header
	//

	var $header      = $('.home-header');
	var $headerMenu  = $('.home-header__mid.home-header__mid_laterfixed');
	var headerMenuHeight = $headerMenu.outerHeight();
	var headerHeight = $header.outerHeight();

	$(document).on('scroll', function(event) {
		if ( /*window.innerWidth > 990*/ true ) {
			var windowHeight = $(window).scrollTop();

			if( windowHeight >= headerHeight ) {
				$headerMenu.addClass('fixed-header');
			} else {
				$headerMenu.removeClass('fixed-header');
			}
		}
	});

	$(window).on('resize', function(e){
		headerMenuHeight = $headerMenu.outerHeight();
		headerHeight     = $header.outerHeight();
	});

	//
	// end fixed header
	//
});

